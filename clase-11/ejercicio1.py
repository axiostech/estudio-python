import sqlite3
productos = [('Impresora',300),('Raton',80),('Ordenador',7500)]

class DataBase:
    def __init__(self, nombreDB):
        self.conexion = sqlite3.connect(nombreDB)
        self.nombreDB = nombreDB
        self.sql = self.conexion.cursor()
    def cerrar(self):
        self.sql.close()
    def crearTabla(self):
        self.sql.execute(f"CREATE TABLE PRODUCTOS (nombre TEXT, precio INTEGER)")
        self.conexion.commit()
    def insertarProductos(self, productos):
        self.sql.executemany("INSERT INTO PRODUCTOS VALUES(?,?)", productos)
        self.conexion.commit()
    def consultarProductos(self):
        self.sql.execute("SELECT * FROM PRODUCTOS")
        info = self.sql.fetchall()
        for producto in info:
            print(producto)
    
database = DataBase("EjercicioDataBase.db")
database.crearTabla()
database.insertarProductos(productos)
database.consultarProductos()
database.cerrar()