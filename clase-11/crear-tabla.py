import sqlite3

try:
    conexion = sqlite3.connect("basedatos1.db")
except:
    print("Error creando o conectando base de datos")

sql = conexion.cursor()

sql.execute("CREATE TABLE PERSONAS (nombre TEXT, apellido TEXT, edad INTEGER)")

conexion.commit()

conexion.close()