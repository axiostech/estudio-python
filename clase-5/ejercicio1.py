diccionario = {
    "manzana":"apple",
    "naranja":"orange",
    "platano":"banana",
    "limon":"lemon"
    }

for key, valor in diccionario.items():
    print(f"English: {valor}, Spanish: {key}.")

diccionario["piña"] = "pineapple"

for fruta in diccionario.items():
    print(fruta)