class Fichero:
    def __init__(self, nombreFichero):
        self.nombreFichero = "./clase-8/" + nombreFichero + ".txt"
    def leer_fichero(self):
        fichero = open(self.nombreFichero, "rt")
        info = fichero.read()
        print(info)
    def grabar_fichero(self):
        return open(self.nombreFichero, "wt")
    def incluir_informacion(self, cadena_texto):
        fichero = open(self.nombreFichero, "at")
        texto = "\n" + cadena_texto
        fichero.write(texto)

