class Silla:
    color = "blanco"
    precio = 100

objetoSilla1 = Silla()

print(objetoSilla1.color)
print(objetoSilla1.precio)

objetoSilla2 = Silla()

objetoSilla2.color = "negro"
objetoSilla2.precio = 150

print(objetoSilla2.color)
print(objetoSilla2.precio)

class Persona:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad
    
    def saludar(self):
        print("Mi nombre es {} y tengo {} años.".format(self.nombre, self.edad))


    

persona = Persona("Daniel Mateus", 23)

persona.saludar()