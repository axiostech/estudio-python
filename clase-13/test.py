def multiplicar(n1, n2):
    return n1 * n2

resultado = multiplicar(2,4)
print(resultado)

import unittest

class Pruebas(unittest.TestCase):
    def test(self):
        self.assertEqual(multiplicar(2,4),8)
        self.assertEqual(multiplicar(3,5),15)
        self.assertEqual(multiplicar(6,4),24)
        self.assertEqual(multiplicar(3,4),12)
        self.assertEqual(multiplicar(1,1),1)

if __name__ == '__main__':
    unittest.main()