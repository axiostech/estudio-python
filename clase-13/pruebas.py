def suma(n1, n2):
    # documentacion y pruebas
    """
    Esta es la documnetacion de esta funcion:
        Recibe dos numeros y devuelve el resultado de la suma
    
    >>> suma(4,3)
    7
    >>> suma(5,4)
    9
    >>> suma(1,3)
    4

    """
    return n1 + n2

resultado = suma(2,4)

print(resultado)

import doctest
doctest.testmod()