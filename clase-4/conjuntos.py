conjunto = {"rojo", "amarillo", "azul"}

print(conjunto)

for color in conjunto:
    print(color)

conjunto.add("negro")

for color in conjunto:
    print(color)