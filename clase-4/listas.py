colores = ["rojo", "amarillo", "verde"]
print(colores[0])
colores.append("naranja")
print(colores)
colores.remove("rojo")
print(colores)

# bucle 
for color in colores:
    print(color)


colores.clear()

print(colores)