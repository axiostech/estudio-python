diccionario = {"red":"rojo", "blue":"azul", "yellow":"amarillo"}
print(diccionario["red"])
diccionario["black"] = "negro"
print(diccionario)
diccionario.pop("yellow")
print(diccionario)

for key, value in diccionario.items():
    print(f"Clave del diccionario es {key}, y su contenido es: {value}")