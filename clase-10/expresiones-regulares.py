import re

texto = "Hola, mi nombre es Daniel Mateus"

busqueda = re.search("Daniel", texto)

if(busqueda):
    print("texto encontrado")
else:
    print("no hay similitudes")

texto2 = """
El Coche de luis es rojo,
EL Coche de Daniel es azul,
El Coche de Carlos es gris
"""

busqueda2 = re.findall("Coche.*azul", texto2)

if(busqueda2):
    print(busqueda2)
else: 
    print("no hay similitudes")


texto3 = "la silla es blanca y vale 100"

resultado = re.sub("blanca","negra", texto3)

print(resultado)
