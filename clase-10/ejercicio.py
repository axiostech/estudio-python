import re

def buscar(palabra, texto):
    resultado = re.search(palabra, texto)
    if(resultado):
        return print(f"La palabra {palabra} si esta en la frase entre las posiciones {resultado.start()} y {resultado.end()}")
    else:
        return print("no hay semejanzas")

buscar("frase","Esto es una frase de pruebas para hacer busqueda")
