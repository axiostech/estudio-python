import numpy as np

def genArray(nmin, nmax):
    if(nmin % 2 == 0):
        array = np.arange(nmin, nmax, 2)
    else:
        nmin = nmin + 1
        array = np.arange(nmin, nmax, 2)
    return array
    
print(genArray(1,20))
print(genArray(2,40))
        

