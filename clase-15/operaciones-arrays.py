import numpy as np

array1 = np.array([1,2,3,4])
array2 = array1 * 2

print(array1)
print(array2)

lista1 = [1,2,3,4]
lista2 = [2,4,6,8]
lista_doble = (lista1,lista2)
array_doble = np.array(lista_doble)

print(array_doble * 2)