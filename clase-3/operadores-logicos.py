numero1 = 10
numero2 = 5

# and, or, not

(numero1 > numero2) and (numero1 != numero2)
(numero1 > numero2) or (numero1 != numero2)
not(numero1 > numero2)
