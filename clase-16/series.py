import pandas as pd

serie1 = pd.Series([3,5,7])

print(serie1)
print(serie1[1])

asignaturas = ["metodos","programacion","algebra"]
notas = [3,5,4]

serie_notas_daniel = pd.Series(notas, index=asignaturas)

print(serie_notas_daniel)
print(serie_notas_daniel[serie_notas_daniel >= 4])

serie_notas_daniel.name = 'Notas de Daniel Mateus'
serie_notas_daniel.index.name = 'Asignaturas de Daniel Mateus'

print(serie_notas_daniel)

diccionario = serie_notas_daniel.to_dict()

print(diccionario)

serie = pd.Series(diccionario)

print(serie)

