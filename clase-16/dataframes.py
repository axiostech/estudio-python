import pandas as pd

lista_notas = [[2.5,3,5,4.1],[4,5,3,4],[2.3,4,2.5,3],[4.1,4.2,3.8,2.8]]
lista_asignaturas = ['Fisica','Algebra','Ciencias', 'Sociales']
lista_nombres = ['Carlos','Daniel','Sofia','Lorein']

dataframe = pd.DataFrame(lista_notas, index=lista_asignaturas, columns=lista_nombres)

print(dataframe)