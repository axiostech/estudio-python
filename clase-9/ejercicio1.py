def operacion(n1, n2, n3):
    try:
        operacion = n1 / (n2 - n3)
        return print(f"Resultado de la operacion {operacion}")
    except ZeroDivisionError:
        return print("Error Division por Cero")
    except:
        return print("Error Desconocido")
    finally:
        return print("Fin de la operacion")

operacion(5,4,2) 
operacion(6,3,3) 