numero1 = 5
numero2 = 0

try:
    division = numero1 / numero2
    print(division)
except ZeroDivisionError:
    print("division por cero - error")
except:
    print("error en la division")
else: 
    print("division correcta")
finally: 
    print("se ejecuta si falla o no")
