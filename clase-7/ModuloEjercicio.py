class Vehiculo:
    def __init__(self,marca,color,combustible,cilindraje):
        self.marca = marca
        self.color = color
        self.combustible = combustible
        self.cilindraje = cilindraje

    def mostrar_vehiculo(self):
        print("************************************************************")
        print(f"EL vehiculo es marca {self.marca}")
        print(f"EL vehiculo es color {self.color}")
        print(f"EL vehiculo utiliza como combustible: {self.combustible}")
        print(f"EL vehiculo es cilindraje: {self.cilindraje}cc")
        print("************************************************************")
        print()

promedio = lambda nota1, nota2, nota3 : (nota1 + nota2 + nota3) / 3
